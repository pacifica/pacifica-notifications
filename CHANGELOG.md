# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.1] - 2019-05-18
### Added
- Event subscription management API
- Event endpoint for notifying subscribers
- Model upgrade process
- ReadtheDocs supported Sphinx docs
- REST API for subscribing to events
  - PUT - Update a Subscription
  - POST - Create a Subscription
  - GET - Get a Subscription
  - DELETE - Delete a Subscription
- REST API for receiving events
  - POST - receive new event

### Changed
